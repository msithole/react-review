FROM node:10-alpine as build

WORKDIR /app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . ./
RUN npm run-script build

# Runtime
FROM nginx:alpine as runtime
EXPOSE 80
WORKDIR /usr/share/nginx/html
COPY --from=build /app/build ./
COPY nginx.conf /etc/nginx/nginx.conf
