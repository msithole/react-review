
export default class MediaQueryService {

	/**
	 * @description Determines the size of the screen and returns small/medium/large/extra-large
	 * @return {string}
	 * @memberof MediaQueryService
	*/
	getMediaQuery() {

		if (window.matchMedia('(max-width: 767px)').matches) {

			return 'small'

		} else if (window.matchMedia('(max-width: 1023px)').matches) {

			return 'medium'

		} else if (window.matchMedia('(max-width: 1280px)').matches) {

			return 'large'

		} else {

			return 'extra-large'

		}

	}

}
