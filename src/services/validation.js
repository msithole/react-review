
export default class ValidationService {

	static email(email) {

		const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return pattern.test(String(email).toLowerCase());

	}

	static required(value) {

		if(value) {

			return true;

		} else {

			return false;

		}

	}

	static idPassport (value) {
	
		return value.length < 13 || value.length > 20;

	}

	static textOnly(value) {

		const pattern = /^[a-z]+$/i;
		return pattern.test(value);

	}

}
