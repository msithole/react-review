
import Countries from 'country-list';

export default class CountryService {

	static countryList = Countries().getNameList();

	static getCountriesArray() {

		return Object.keys(this.countryList).map(i => {

			return {

				value: this.countryList[i].toLowerCase(),
				label: i

			}

		});

	}

}
