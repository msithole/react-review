
import Four04 from './components/404/404';
import Five00 from './components/500/500';
import Home from './components/home/home';
import BondAdvisor from './components/bond-advisor/bond-advisor';
import ContinueApplication from './components/continue-application/contintue-application';
import personalDetailsRoutes from './routes/personal-details';
import qualificationsRoutes from './routes/qualifications';

const routeConfig = [
	{
		path: '/',
		exact: true,
		component: Home
	},
	{
		path: '/bond-advisor',
		component: BondAdvisor
	},
	...personalDetailsRoutes,
	...qualificationsRoutes,
	{
		path: '/server-error',
		component: Five00
	},
	{
		path: '/continue-application',
		component: ContinueApplication
	},
	{
		component: Four04
	}
];

export default routeConfig;
