
import {
	CURRENT_PAGE_ID,
	SET_IS_POSTING,
	SHOW_HEADER,
	ACCOUNT_CREATED,
	SERVER_DATA_UPDATED,
	SHOW_BACK_BUTTON,
	SHOW_SAVE_BUTTON,
	UPDATE_PROGRESS,
	UPDATE_PROGRESS_LOCATION,
	UPDATE_STATE_DATA,
	PLAYED_ANIMATIONS,
	CONTINUE_APPLICATION
} from '../actions/action-types';

const initState = {
	showHeader: false,
	showBackButton: false,
	showSaveButton: true,
	isFetching: false,
	isPosting: false,
	stepReceived: null,
	currentPageId: null,
	progress: 0,
	progressLocation: '',
	form: {},
	animationsPlayed: {},
	token: null
}

export default function appReducer(state = initState, action) {

	switch (action.type) {

		case CURRENT_PAGE_ID:
			return {
				...state,
				currentPageId: action.pageId
			}

		case SET_IS_POSTING:
			return {
				...state,
				isPosting: action.isPosting
			}

		case SHOW_HEADER:
			return {
				...state,
				showHeader: action.toggle
			}

		case SHOW_BACK_BUTTON:
			return {
				...state,
				showBackButton: action.toggle
			}

		case SHOW_SAVE_BUTTON:
			return {
				...state,
				showSaveButton: action.toggle
			}
		
		case ACCOUNT_CREATED:
			return {
				...state,
				token: action.data.data,
				isPosting: false
			}

		case SERVER_DATA_UPDATED:
			return {
				...state,
				isPosting: false
			}

		case UPDATE_PROGRESS:
			return {
				...state,
				progress: action.progress
			}

		case UPDATE_PROGRESS_LOCATION:
			return {
				...state,
				progressLocation: action.progressLocation
			}

		case UPDATE_STATE_DATA:
			return {
				...state,
				form: {
					...state.form,
					...action.data
				},
				isPosting: true
			}
	
		case CONTINUE_APPLICATION:
			return {
				...state,
				form: {
					...state.form,
					...action.data.form
				},
				token: action.data.token,
				isFetching: false
			}

		case PLAYED_ANIMATIONS:
			return {
				...state,
				animationsPlayed: {
					...state.animationsPlayed,
					[action.animationId]: action.animationId
				}
			}

		default:
			return state

	}

}
