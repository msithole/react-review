
export const SHOW_HEADER = 'showHeader';
export const SHOW_BACK_BUTTON = 'showBackButton';
export const SHOW_SAVE_BUTTON = 'showSaveButton';
export const UPDATE_PROGRESS = 'updateProgress';
export const UPDATE_PROGRESS_LOCATION = 'updateProgressLocation';
export const PLAYED_ANIMATIONS = 'playedAnimations';
export const CURRENT_PAGE_ID = 'currentPageId';

export const SET_IS_FETCHING = 'setIsFetching';
export const SET_IS_POSTING = 'setIsPosting';
export const DATA_POSTED = 'dataPosted';
export const POST_DATA = 'postData';
export const SERVER_DATA_UPDATED = 'serverDataUpdated';
export const ACCOUNT_CREATED = 'accountCreated';
export const CREATE_ACCOUNT = 'createAccount';
export const UPDATE_STATE_DATA = 'updateStateData';
export const SAVE_FOR_LATER = 'saveForLater';
export const CONTINUE_APPLICATION = 'continueApplication';
