
import { push } from 'connected-react-router';

import {
	SHOW_HEADER,
	CURRENT_PAGE_ID,
	SHOW_BACK_BUTTON,
	SHOW_SAVE_BUTTON,
	UPDATE_STATE_DATA,
	ACCOUNT_CREATED,
	CONTINUE_APPLICATION,
	SERVER_DATA_UPDATED,
	UPDATE_PROGRESS,
	SET_IS_FETCHING,
	SET_IS_POSTING,
	PLAYED_ANIMATIONS,
	UPDATE_PROGRESS_LOCATION
} from './action-types';

const apiBaseUrl = 'https://bondspark-api--develop.dev-kube.leadhome.co.za/api';


export function toggleHeader(toggle) {

	return {
		type: SHOW_HEADER,
		toggle
	}

}

export function toggleBackButton(toggle) {

	return {
		type: SHOW_BACK_BUTTON,
		toggle
	}

}

export function toggleSaveButton(toggle) {

	return {
		type: SHOW_SAVE_BUTTON,
		toggle
	}

}

export function setCurrentPageId(pageId) {

	return {
		type: CURRENT_PAGE_ID,
		pageId
	}

}

export function setIsFetching(isFetching) {

	return {
		type: SET_IS_FETCHING,
		isFetching
	}

}

export function setIsPosting(isPosting) {

	return {
		type: SET_IS_POSTING,
		isPosting
	}

}

export function updateProgress(progress) {

	return {
		type: UPDATE_PROGRESS,
		progress
	}

}

export function updateProgressLocation(progressLocation) {

	return {
		type: UPDATE_PROGRESS_LOCATION,
		progressLocation
	}

}

export function accountCreated(data) {

	return {
		type: ACCOUNT_CREATED,
		data
	}

}

export function applicationContinued(data) {

	return {
		type: CONTINUE_APPLICATION,
		data
	}

}

export function serverDataUpdated(data) {

	return {
		type: SERVER_DATA_UPDATED,
		data
	}

}

export function updateStateData(data) {

	return {
		type: UPDATE_STATE_DATA,
		data
	}

}

export function setPlayedAnimations(animationId) {

	return {
		type: PLAYED_ANIMATIONS,
		animationId
	}

}

export function postData(data, endpoint) {

	return async (dispatch, getState) => {

		dispatch(updateStateData(data));

		switch (getState().app.currentPageId) {

			case 'idNumber' :
				dispatch(creatAccount(data));
				break;

			default:
				dispatch(sendToServer(data, endpoint));

		}
	
	}

}

export function creatAccount(data) {

	return async (dispatch) => {

		fetch(`${apiBaseUrl}/customer/apply`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Basic '+ btoa('bondi:b0nd1b0ndBOND')
			},
			body: JSON.stringify(data)
		})
		.then((resp) => resp.json())
		.then((response) => dispatch(accountCreated(response)));
	
	}

}

export function sendToServer(data, endpoint) {

	return async (dispatch, getState) => {

		fetch(`${apiBaseUrl}/${endpoint}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${getState().app.token}`,
				'Progress': getState().app.progressLocation
			},
			body: JSON.stringify(data)
		})
		.then((resp) => resp.json())
		.then((data) => dispatch(serverDataUpdated(data)));
	
	}

}

export function saveForLater() {

	return async (dispatch, getState) => {

		dispatch(setIsPosting(true));

		fetch(`${apiBaseUrl}/application/saveforlater`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${getState().app.token}`,
				'Progress': getState().app.progressLocation
			},
			body: JSON.stringify({})
		})
		.then((resp) => resp.json())
		.then(() => dispatch(setIsPosting(false)));
	
	}

}

export function continueApplication(Token) {

	function mapData(resp) {

		return {
			form: {
				anyOtherApplicants: resp.data.application.anyOtherApplicants,
				anyoneElseApplying: resp.data.application.anyoneElseApplying,
				howManyApplicants: resp.data.application.howManyApplicants,
				email: resp.data.application.customers[0].email,
				idNumber: resp.data.application.customers[0].idNumber,
				maritalStatus: resp.data.application.customers[0].maritalStatus,
				name: resp.data.application.customers[0].name,
				nationality: resp.data.application.customers[0].nationality,
				permanentResident: resp.data.application.customers[0].permanentResident,
				saCitizen: resp.data.application.customers[0].saCitizen,
				surname: resp.data.application.customers[0].surname,
				description: resp.data.application.addresses[0].description,
				fullAddress: resp.data.application.addresses[0].fullAddress,
				residenceStatus: resp.data.application.addresses[0].residenceStatus,
				addressDuration: {
					years: resp.data.application.addresses[0].livingYears,
					months: resp.data.application.addresses[0].livingMonths
				},
				employmentStatus: resp.data.employmentHistory.currentEmployment,
				currentEmployerName: resp.data.employmentHistory.currentEmployerName,
				currentPosition: resp.data.employmentHistory.currentPosition,
				currentEmployerContact: resp.data.employmentHistory.currentEmployerContact,
				currentEmployerAddress: resp.data.employmentHistory.currentEmployerAddress,
				currentDuration: {
					years: resp.data.employmentHistory.currentEmploymentPeriodYears,
					months: resp.data.employmentHistory.currentEmploymentPeriodMonths,
				},
				previousEmployed: resp.data.employmentHistory.previousEmployed,
				previousEmployer: resp.data.employmentHistory.previousEmployer,
				previousPosition: resp.data.employmentHistory.previousPosition,
				previousEmployerContact: resp.data.employmentHistory.previousEmployerContact,
				previousEmployerAddress: resp.data.employmentHistory.previousEmployerAddress,
				previousDuration: {
					years: resp.data.employmentHistory.previousEmploymentPeriodYears,
					months: resp.data.employmentHistory.previousEmploymentPeriodMonths,
				},
				qualificationLevel: resp.data.qualification.qualificationLevel,
				studyLevel: resp.data.qualification.studyLevel,
				qualificationDescription: resp.data.qualification.description
			},
			token: resp.info
		};

	}

	return async (dispatch, getState) => {

		dispatch(setIsFetching(true));

		fetch(`${apiBaseUrl}/application/continue`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Progress': getState().app.progressLocation
			},
			body: JSON.stringify({Token})
		})
		.then((resp) => resp.json())
		.then(function(response) {
	
			dispatch(applicationContinued(mapData(response)));
			dispatch(push(response.data.application.progress))

		});
	
	}

}

