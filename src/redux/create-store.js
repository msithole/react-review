
import {
	createStore,
	compose,
	applyMiddleware
} from 'redux';
import { routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history'

import baseReducer from './reducers';

/* eslint-disable no-underscore-dangle */

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const history = createBrowserHistory();

const store = createStore(
	baseReducer(history),
	composeEnhancer(applyMiddleware(routerMiddleware(history), thunk)),
);

/* eslint-enable */

export default store;
