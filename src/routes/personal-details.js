
import IdPassport from '../components/personal-details/id-passport/id-passport';
import EmailAddress from '../components/personal-details/email-address/email-address';
import NameSurname from '../components/personal-details/name-surname/name-surname';
import WeddingBells from '../components/personal-details/wedding-bells/wedding-bells';
import MultipleApplicants from '../components/personal-details/multiple-applicants/multiple-applicants';
import BesidesSpouse from '../components/personal-details/besides-spouse/besides-spouse';
import Citizenship from '../components/personal-details/citizenship/citizenship';
import MoreApplicants from '../components/personal-details/more-applicants/more-applicants';
import Residency from '../components/personal-details/residency/residency';
import Nationality from '../components/personal-details/nationality/nationality';
import Address from '../components/personal-details/address/address';
import LivingArrangements from '../components/personal-details/living-arrangements/living-arrangements';
import AddressDuration from '../components/personal-details/address-duration/address-duration';
import OtherArrangments from '../components/personal-details/other-arrangments/other-arrangments';

const personalDetailsRoutes = [
	{
		path: '/personal-details/id-passport',
		component: IdPassport
	},
	{
		path: '/personal-details/email-address',
		component: EmailAddress
	},
	{
		path: '/personal-details/name-surname',
		component: NameSurname
	},
	{
		path: '/personal-details/wedding-bells',
		component: WeddingBells
	},
	{
		path: '/personal-details/multiple-applicants',
		component: MultipleApplicants
	},
	{
		path: '/personal-details/besides-spouse',
		component: BesidesSpouse
	},
	{
		path: '/personal-details/citizenship',
		component: Citizenship
	},
	{
		path: '/personal-details/more-applicants',
		component: MoreApplicants
	},
	{
		path: '/personal-details/residency',
		component: Residency
	},
	{
		path: '/personal-details/nationality',
		component: Nationality
	},
	{
		path: '/personal-details/address',
		component: Address
	},
	{
		path: '/personal-details/living-arrangements',
		component: LivingArrangements
	},
	{
		path: '/personal-details/address-duration',
		component: AddressDuration
	},
	{
		path: '/personal-details/other-arrangments',
		component: OtherArrangments
	}
];

export default personalDetailsRoutes;
