
import HighestQualification from '../components/qualifications/highest-qualification/highest-qualification';
import Grade from '../components/qualifications/grade/grade';
import DiplomaDuration from '../components/qualifications/diploma-duration/diploma-duration';
import DescribeQualification from '../components/qualifications/describe-qualification/describe-qualification';
import EmploymentStatus from '../components/qualifications/employment-status/employment-status';
import Employer from '../components/qualifications/employer/employer';
import Company from '../components/qualifications/company/company';
import CompanyDuration from '../components/qualifications/company-duration/company-duration';
import ContractDuration from '../components/qualifications/contract-duration/contract-duration';
import PreviousEmployer from '../components/qualifications/previous-employer/previous-employer';
import PreviousEmployerDetails from '../components/qualifications/previous-employer-details/previous-employer-details';
import PreviousCompany from '../components/qualifications/previous-company/previous-company';
import PreviousCompanyDuration from '../components/qualifications/previous-company-duration/previous-company-duration';

const qualificationsRoutes = [
	{
		path: '/qualifications/highest-qualification',
		component: HighestQualification
	},
	{
		path: '/qualifications/pre-matric',
		component: Grade
	},
	{
		path: '/qualifications/diploma-duration',
		component: DiplomaDuration
	},
	{
		path: '/qualifications/describe-qualification',
		component: DescribeQualification
	},
	{
		path: '/qualifications/employment-status',
		component: EmploymentStatus
	},
	{
		path: '/qualifications/employer',
		component: Employer
	},
	{
		path: '/qualifications/company',
		component: Company
	},
	{
		path: '/qualifications/company-duration',
		component: CompanyDuration
	},
	{
		path: '/qualifications/contract-duration',
		component: ContractDuration
	},
	{
		path: '/qualifications/previous-employer',
		component: PreviousEmployer
	},
	{
		path: '/qualifications/previous-employer-details',
		component: PreviousEmployerDetails
	},
	{
		path: '/qualifications/previous-company',
		component: PreviousCompany
	},
	{
		path: '/qualifications/previous-company-duration',
		component: PreviousCompanyDuration
	},
	
]

export default qualificationsRoutes;
