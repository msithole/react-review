
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
	Route,
	Switch
} from "react-router-dom";

import {
	TransitionGroup,
	CSSTransition
} from "react-transition-group";

import './app.scss';
import routerConfig from './route-config';
import StartLoader from './components/start-loader/start-loader';
import Header from './components/header/header';

class App extends Component {

	state = {
		loading: true
	}

	componentDidMount() {

		setTimeout(() => {
			
			this.setState({
				loading: false
			})

		}, 2500);

	}

	render() {

		return (

			<>

				{this.state.loading ? <StartLoader/> :
				
				<>

					{this.props.app.showHeader ? <Header /> : null}

					<Route render={({location}) => (

						<TransitionGroup>

							<CSSTransition
								key={location.key}
								timeout={500}
								classNames="fade">

								<Switch location={location}>

									{routerConfig.map((route, i) => (

										<Route key={i}
											{...route} />

									))}

								</Switch>

							</CSSTransition>
							
						</TransitionGroup>

					)} />

				</>

				}

			</>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default withRouter(connect(mapStateToProps)(App));

