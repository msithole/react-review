
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Parser from 'html-react-parser';

import {toggleSaveButton} from '../../redux/actions/app-actions';
import Box from '../box/box';
import './wizard-box.scss';

class WizardBox extends Component {

	componentDidMount() {
		this.props.dispatch(toggleSaveButton(false));
	}

	componentWillUnmount() {
		this.props.dispatch(toggleSaveButton(true));
	}

	render() {

		return (

			<>

				<Box data={this.props.data.box}/>

				<p className="disclaimer">
					{this.props.data.footer.top ? this.props.data.footer.top : ''}
				</p>
				
				<p className="disclaimer">
					{Parser(this.props.data.footer.bottom ? this.props.data.footer.bottom : '')}
				</p>

			</>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default connect(mapStateToProps)(WizardBox);
