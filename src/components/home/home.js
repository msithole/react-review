
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import Parser from 'html-react-parser';

import { toggleHeader } from '../../redux/actions';
import Button from '../button/button';
import  Animation from '../animation/animation';
import panel from '../../assets/image/intro-background-desktop.png';
import logo from '../../assets/image/logo.svg';

import './home.scss';

class Home extends Component {

	state = {
		header: null,
		body: null,
		loadImage: false
	};

	constructor(props) {

		super(props);
		this.animationElement = React.createRef();

	}

	componentDidMount() {

		fetch('/data/home.json')
			.then(response => response.json())
			.then(data => this.setState({
				header: data.header,
				body: data.body,
				footer: data.footer
			}));

		this.props.dispatch(toggleHeader(false));	

		setTimeout(() => {
			
			this.setState({
				loadImage: true
			})

		}, 800);

	}

	componentWillUnmount() {

		this.props.dispatch(toggleHeader(true));

	}

	render() {

		return (

			<section className="home page">

				<div className="home__content d-flex d-md-none flex-column justify-content-around">

					{this.state.loadImage ? 
				
						<Animation {...this.state.body.animationConfig}
							modifier="home" />

					: null}

					<header className="home__header d-flex flex-column align-items-center">

						<img src={logo} 
							className="home__logo"
							alt="Bondspark"/>

						<h1 className="home__title">

							{this.state.header ? this.state.header.title : ''}

							<div className="semi-bold">
								{this.state.header ? this.state.header.subTitle : ''}
							</div>

						</h1>

						<div className="home__time d-flex">

						 	<i className="material-icons home__icon">
								query_builder
							</i>

							{this.state.header ? this.state.header.duration : '' } mins

						</div>

					</header>

					<div className="home__body d-flex flex-column align-items-center">

						<p className="semi-bold">
							{this.state.body ? this.state.body.title : ''}
						</p>

						<p>
							{this.state.body ? this.state.body.copy : ''}
						</p>

						<Link to="/personal-details/id-passport">
							
							<Button 
								text={this.state.body ? this.state.body.button.text : ''}
								type={this.state.body ? this.state.body.button.type : ''}
								icon={this.state.body ? this.state.body.button.icon : ''}>
							</Button>

						</Link>

					</div>
				
				</div>

				<div className="home__content d-none d-md-flex flex-column align-items-center justify-content-center">

					<img src={logo} 
						className="home__logo"
						alt="Bondspark"/>

					<div className="home__box">

						<div className="row">

							<div className="col-6 col-lg-5">

								<img src={panel} 
									className="home__panel"
									alt="Bondspark"/>

							</div>

							<div className="col-6 col-lg-7">

								<div className="home__box-content d-flex flex-column justify-content-around">

									<header className="home__header d-flex flex-column align-items-center">

										<h1 className="home__title">

											{this.state.header ? this.state.header.title : ''}

											<div className="semi-bold">
												{this.state.header ? this.state.header.subTitle : ''}
											</div>

										</h1>

										</header>

										<div className="home__body d-flex flex-column align-items-center">

										<p className="semi-bold">
											{this.state.body ? this.state.body.title : ''}
										</p>

										<p>
											{this.state.body ? this.state.body.copy : ''}
										</p>

										<Link to="/personal-details/id-passport">
											
											<Button 
												text={this.state.body ? this.state.body.button.text : ''}
												type={this.state.body ? this.state.body.button.type : ''}
												icon={this.state.body ? this.state.body.button.icon : ''}>
											</Button>

										</Link>

									</div>

									<div className="home__time d-flex  justify-content-center">

									 	<i className="material-icons home__icon">
											query_builder
										</i>

										{this.state.header ? this.state.header.duration : '' } mins

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<footer className="home__footer d-flex flex-column align-items-center">
					<label>
						{Parser(this.state.footer ? this.state.footer.note: '' )}
					</label>
				</footer>

			</section>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(Home)
