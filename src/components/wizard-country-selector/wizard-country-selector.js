
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {postData} from '../../redux/actions/app-actions';
import Button from '../button/button';
import CountrySelector from '../country-selector/country-selector';
import './wizard-country-selector.scss';

class WizardCountrySelector extends Component {

	state = {
		country: {
			value: 'zw',
			label: 'Zimbabwe'
		},
		showCountries: false
	}

	constructor(props) {
	
		super(props);

		this.onClick = this.onClick.bind(this);
		this.onChange = this.onChange.bind(this);

	}

	componentDidMount() {

		if(this.props.form.hasOwnProperty('Country')) {

			this.setState({
				country: this.props.form['Country']
			});

		}

		this.setState({
			showCountries: true
		});

	}


	onChange(selectedCountry) {

		this.setState({
			country: selectedCountry
		});

	}

	onClick() {

		this.props.dispatch(postData({
			Country: this.state.country
		}, this.props.data.endpoint || ''));

		this.props.history.push(`${this.props.data.destination}`);

	}

	render() {

		return (

			<div className="form-step__item">

				<p className="form__label">
					{this.props.data.title}
				</p>

				{this.state.showCountries && 
				
					<CountrySelector
						selectedOption={this.state.country}
						onChange={this.onChange}/>

				}

				<div className="form-step__item d-flex justify-content-end"
					onClick={this.onClick}>

					<Button {...this.props.data.button}/>

				</div>

			</div>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(WizardCountrySelector));
