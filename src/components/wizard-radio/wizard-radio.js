
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {postData} from '../../redux/actions/app-actions';
import Button from '../button/button';
import './wizard-radio.scss';

class WizardRadio extends Component {

	state = {
		selectedOption: null
	}

	constructor(props) {
	
		super(props);

		this.onClick = this.onClick.bind(this);

	}

	componentDidMount() {

		if(this.props.form.hasOwnProperty(this.props.data.name)) {

			this.setState({
				selectedOption: this.props.form[this.props.data.name]
			});

		}

	}

	getSelectedValue () {

		if(this.props.form.hasOwnProperty(this.props.data.name)) {

			this.setState({
				selectedOption: this.props.form[this.props.data.name]
			});

		}

	}

	onClick(item, e) {

		this.props.dispatch(postData({
			[this.props.data.name]: item.id
		}, this.props.data.endpoint || ''));
		
		this.props.history.push(`${item.destination}`);
	
	}

	render() {

		const renderRadios = this.props.data.actions.map((item, index) => 
		
			<div className="d-flex justify-content-end justify-content-lg-center"
				key={index}>

				<Button {...item}
					selectedOption={this.state.selectedOption}
					onClick={(e) => this.onClick(item, e)} />

			</div>

		);

		return (

			<div className="form-step__item">
				{renderRadios}
			</div>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(WizardRadio));
