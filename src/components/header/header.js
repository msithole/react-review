
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { saveForLater } from '../../redux/actions/app-actions';
import Button from '../button/button';
import Tooltip from '../tooltip/tooltip';
import ProgressBar from '../progress-bar/progress-bar';
import './header.scss';
class Header extends Component {

	state = {
		
		save: {
			type: 'label',
			text: 'Save & Exit',
			id: "save",
			icon: {
				name: 'cloud_upload',
				type: 'label',
				position: 'right'
			}
		},
		back: {
			type: 'label',
			text: 'Back',
			id: "back",
			icon: {
				name: 'arrow_back',
				type: 'label',
				position: 'left'
			}
		}

	};

	constructor(props) {

		super(props);

		this.stepBack = this.stepBack.bind(this);
		this.saveApplication = this.saveApplication.bind(this);

	}

	stepBack() {

		window.history.back();

	}

	saveApplication() {

		this.props.dispatch(saveForLater());

	}

	render() {

		return (

			<header className="header">
				
				<ProgressBar />

				<div className="row">

					<div className="col col-6">

						<div className={this.props.app.showBackButton ? 'header__nav-item' : 'header__nav-item header__nav-item--hidden'}
							onClick={this.stepBack}>
					
							<Button 
								text={this.state.back ? this.state.back.text : '' }
								id={this.state.back ? this.state.back.id : '' }
								type={this.state.back ? this.state.back.type : '' }
								icon={this.state.back ? this.state.back.icon : '' }>
							</Button>

						</div>
					
					</div>

					<div className="col col-6 d-flex justify-content-end">

						<div className={this.props.app.showSaveButton ? 'header__nav-item' : 'header__nav-item header__nav-item--hidden'}
							onClick={this.saveApplication}>
						
							<Button 
								text={this.state.save ? this.state.save.text : '' }
								type={this.state.save ? this.state.save.type : '' }
								id={this.state.save ? this.state.save.id : '' }
								icon={this.state.save ? this.state.save.icon : '' }>
							</Button>

						</div>

						<Tooltip></Tooltip>

					</div>
				
				</div>
				
			</header>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(Header)
