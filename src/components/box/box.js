
import React, { Component } from 'react';
import Parser from 'html-react-parser';
import { NavLink } from "react-router-dom";

import './box.scss';

class Box extends Component {

	render() {

		const boxItems = this.props.data.points.map((item, index) => 
		
			<li className="box__list-item"
				key={index}>

				<i className={item.icon === 'done' ? 'material-icons box__list-icon box__list-icon--correct' : 'material-icons box__list-icon box__list-icon--incorrect'}>
					{item.icon}
				</i>

				<span>

					{Parser(item.text, {

							replace: function(domNode) {

								if (domNode.name ==='navlink' && domNode.type === 'tag') {

									return <NavLink
										exact
										to={domNode.attribs.href}>

										{domNode.children.map((child) => child.data)}
										
									</NavLink>
								}

							}
						}

					)}
				</span>

			</li>

		);

		return (

			<div className="box">

				<div className="box__title">
					{this.props.data.title}
				</div>

				<ul className="box__list">
					{boxItems}
				</ul>
				
			</div>

		);

	}

}

export default Box;
