
import React,{ Component } from 'react';

import logo from '../../assets/image/logo.svg';
import background from '../../assets/image/error-404-background.svg';

class Four04 extends Component {

	render() {

		return (

			<div className="error page">

				<div className="error__content d-flex flex-column justify-content-around">

					<div>
						<img src={background} 
							className="error__background"
							alt="Bondspark"/>

						<h2 className="error__title">
							To infinity and... ooops... 
							<div className="semi-bold">page not found.</div>
						</h2>

						<label>404 error</label>
					</div>

				</div>

				<footer className="error__footer d-flex flex-column align-items-center">
					<img src={logo} 
						className="error__logo"
						alt="Bondspark"/>
				</footer>

			</div>

		);

	}

}

export default Four04;
