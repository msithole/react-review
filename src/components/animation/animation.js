
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as lottie from 'lottie-web';

import { setPlayedAnimations } from '../../redux/actions/app-actions';
import './animation.scss';

class Animaiton extends Component {

	animatedCount = 0;

	constructor(props) {

		super(props);
		this.animationElement = React.createRef();

	}

	componentDidMount() {}

	init() {

		if(this.animatedCount < 1) {

			new lottie.loadAnimation({
				container: this.animationElement.current,
				renderer: 'svg',
				loop: this.props.loop || false,
				autoplay: true,
				path: this.props.file
			});
			
			if(this.props.id) {
				this.props.dispatch(setPlayedAnimations(this.props.id));
			}

		}

		this.animatedCount += 1;

	}

	render() {
		
		setTimeout(() => {
			
			this.init();

		}, 500);

		return (

			<div className={`animation animation--${this.props.modifier}`}
				ref={this.animationElement}>
			</div>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default connect(mapStateToProps)(Animaiton);
