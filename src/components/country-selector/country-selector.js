
import React,{ Component } from 'react';
import Select, { components } from 'react-select';

import CountryService from '../../services/countries';
import './country-selector.scss';
import '../../../node_modules/flag-icon-css/css/flag-icon.min.css';

const countryList = CountryService.getCountriesArray();

class CountrySelector extends Component {

	render() {

		const SingleValue = ({ children, ...props }) => (

			<components.SingleValue {...props}>
				<span className={`flag-icon flag-icon-${props.selectProps.defaultValue.value}`}></span> 
				<span className='country-selector__text'>
					{props.selectProps.defaultValue.label}
				</span>
			</components.SingleValue>

		);

		const Option = ({ children, ...props }) => (

			<components.Option {...props}>
				<span className={`flag-icon flag-icon-${props.value}`}></span> 
				<span className='country-selector__text'>
					{children}
				</span>
			</components.Option>

		);

		return (

			<>

				<Select
					classNamePrefix="country-selector"
					defaultValue={this.props.selectedOption}
					cacheOptions={true}
					placeholder="Start typing country..."
					isSearchable={true}
					name="country"
					components={{ SingleValue, Option}}
					onChange={this.props.onChange}
					options={countryList}/>

			</>

		);

	}

}

export default CountrySelector;
