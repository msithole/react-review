
import React,{ Component } from 'react';

import logo from '../../assets/image/logo.svg';
import background from '../../assets/image/error-500-background.svg';

class Five00 extends Component {

	render() {

		return (

			<div className="error page">

				<div className="error__content d-flex flex-column justify-content-around">

					<div>
	
						<img src={background} 
							className="error__background"
							alt="Bondspark"/>

						<h2 className="error__title">

							Whooaaah! that was one jenga block too many 

							<div className="semi-bold">
								something fell over.
							</div>

						</h2>

						<label>500 error</label>

					</div>

				</div>

				<footer className="error__footer d-flex flex-column align-items-center">

					<img src={logo} 
						className="error__logo"
						alt="Bondspark"/>

				</footer>

			</div>

		);

	}

}

export default Five00;
