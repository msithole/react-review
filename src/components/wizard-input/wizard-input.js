
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { postData } from '../../redux/actions/app-actions';
import Input from '../input/input';
import Button from '../button/button';
import './wizard-input.scss';

class WizardInput extends Component {

	state = {
		form: {},
		fields: {},
		isStepValid: false,
		showHint: false,
		showValidation: false
	}

	constructor(props) {

		super(props);

		this.onChange = this.onChange.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onBlur = this.onBlur.bind(this);

	}

	componentDidMount() {

		const fields = {};
		const form = this.props.form;

		this.props.data.actions.map(field => {
			return fields[field.id] = form[field.id] ? 'pass' : null
		});

		this.setState({
			form,
			fields,
			isStepValid: this.validateStep(fields)
		});

		if(this.props.location.pathname === '/personal-details/id-passport') {

			this.setState({
				showHint: true
			});

		} else {

			this.setState({
				showHint: false
			});

		}

	}

	onChange(e) {

		this.setState({
			form: {
				...this.state.form,
				[e.target.id]: e.target.value
			}
		});

	}

	onBlur(e) {

		let updatedFields;

		if(e.target.type === 'text') {

			updatedFields = {
				...this.state.fields,
				[e.target.id]: e.target.value.length >= 2 ? 'pass' : 'fail' 
			}

		}

		if(e.target.type === 'email') {

			const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			updatedFields = {
				...this.state.fields,
				[e.target.id]: pattern.test(String(e.target.value).toLowerCase()) ? 'pass' : 'fail' 
			}

		}

		this.setState({
			isStepValid: this.validateStep(updatedFields),
			fields: updatedFields
		});
	
	}

	onClick() {

		this.setState({
			showValidation: true
		})

		if(this.state.isStepValid) {

			this.props.dispatch(postData(this.state.form, this.props.data.endpoint || ''));
			this.props.history.push(`${this.props.data.destination}`);

		}

	}

	validateStep(fields) {

		let valid = true;
		
		Object.keys(fields).forEach((field) => {

			if(fields[field] !== 'pass'){
				valid = false;
			}

		});

		return valid;

	}

	onKeyDown = (e) => {

		if (e.key === 'Enter') {
			this.onClick();
		}

	}

	onKeyUp = (e) => {

		const el = e.currentTarget;

		if(el.id === 'idNumber' && el.value.length === 13){

			this.setState({
				isStepValid: true
			});
			
		} else {

			this.setState({
				isStepValid: false
			});

		}

	}

	render() {

		const renderInputs = this.props.data.actions.map((input, index) => 

			<div className="form__item"
				key={index}>
	
				<Input {...input}
					onChange={this.onChange}
					value={this.state.form[input.id]}
					onBlur={this.onBlur}
					onKeyDown={this.onKeyDown}
					onKeyUp={this.onKeyUp}/>

				{(input.validate && this.state.fields[input.id]) || (input.validate && this.state.showValidation) ? 

					<div className={this.state.fields[input.id] === 'pass' ? 'form__validation form__validation--valid' : 'form__validation form__validation--invalid'}>

						<div className="form__validation-indicator">

							<i className="material-icons form__validation-icon">
								{this.state.fields[input.id] === 'pass' ? 'done' : 'close'}
							</i>

						</div>

						{input.validation[this.state.fields[input.id]] || 'This field is required'}

					</div>
				
				: null} 

			</div>

		);

		return (

			<div className="wizard-input">

				<div className="row">

					<div className="col col-md-12">

						<div className="form d-md-flex align-items-md-center flex-md-column">
							{renderInputs}
						</div>

					</div>

					<div className="col col-md-12 d-flex align-items-center justify-content-md-end" 
						onClick={this.onClick}>

						{this.state.showHint ? 
						
							<div className="instruction">

								<label>
									<i className="material-icons instruction__icon">
										subdirectory_arrow_right
									</i>
									Press enter to submit
								</label>

							</div>

						: null}

						<div className={this.props.data.actions.length > 1 ? 'form__multiple-input' : ''}>

							<Button {...this.props.data.button}
								disabled={!this.state.isStepValid} />

						</div>

					</div>

				</div>

			</div>

		);

	}

}


function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(WizardInput));
