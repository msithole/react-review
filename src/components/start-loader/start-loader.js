
import React, { Component } from 'react';
import * as lottie from 'lottie-web';

import './start-loader.scss';
import logo from '../../assets/image/logo.svg';

class StartLoader extends Component {

	componentDidMount() {}

	render() {

		return (

			<div className="start-loader d-flex flex-column align-content-center">
				
				<div className="start-loader__logo">
					<img src={logo}
						alt="logo"/>
				</div>

				<div className="start-loader__loading">
					<div className="start-loader__loaded"></div>
				</div>

				<div className="start-loader__text">
					<label className="semi-bold">
						get ready...
					</label>
				</div>
				
			</div>

		);

	}

}

export default StartLoader;
