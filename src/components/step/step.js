
import React,{ Component } from 'react';
import { connect } from 'react-redux';

import {
	toggleHeader,
	toggleBackButton,
	setCurrentPageId
} from '../../redux/actions/app-actions';

import Animation from '../animation/animation';

import './step.scss';

class Step extends Component {

	state = {
		content: null,
		pageId: null
	}
	
	componentDidMount() {

		this.toggleHeader();

		this.props.dispatch(setCurrentPageId(this.props.input.name));

	}

	toggleHeader() {

		if(!this.props.app.showHeader) {
			this.props.dispatch(toggleHeader(true));
		}

		setTimeout(() => {

			this.props.app.currentPageId !== 'IdPassport' ? this.props.dispatch(toggleBackButton(true)) : this.props.dispatch(toggleBackButton(false));

		}, 1000);

	}

	render() {
		
		return (

			<section className="personal-details d-flex flex-column justify-content-center align-items-center page">

				<Animation id={this.props.input.id}
					file={this.props.input.animation}/>

				<h2 className="personal-details__title">

					{this.props.input.intro}

					<div className="semi-bold">
						{this.props.input.question}
					</div>

				</h2>

				{this.props.children}

			</section>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default connect(mapStateToProps)(Step);
