
import React, { Component } from 'react';

import './input.scss';

class Input extends Component {

	render() {

		return (

			<>

				{this.props.label && 

					<label className="form__input-label">
						{this.props.label}
					</label>

				}

				<input className="form__input"
					value={this.props.value || ''}
					type={this.props.type}
					id={this.props.id}
					name={this.props.id}
					onChange={this.props.onChange}
					onBlur={this.props.onBlur}
					onKeyDown={this.props.onKeyDown}
					onKeyUp={this.props.onKeyUp}
					placeholder={this.props.placeholder} />

			</>

		);

	}

}

export default Input;
