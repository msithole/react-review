
import React, { Component } from 'react';
import WizardBox from '../wizard-box/wizard-box';
import Step from '../step/step';

class BondAdvisor extends Component {

	state = {};

	render() {

		const data = {
			"animation": "/animation/animation6.json",
			"name": "bondAdvisor",
			"id": 8,
			"intro": "We can't fit that piece in right now,",
			"question": "A bond advisor will take your special case from here.",
			"type": "box",
			"box": {
				"title": "What can I do?",
				"points": [
					{
						"icon": "done",
						"text": "Stay calm and relax :)"
					},
					{
						"icon": "done",
						"text": "Wait for the bond advisor to contact you"
					},
					{
						"icon": "done",
						"text": "Check your affordability <Navlink href='/personal-details/id-passport'>here</Navlink>"
					},
					{
						"icon": "close",
						"text": "Throw your phone at the wall"
					},
					{
						"icon": "close",
						"text": "Panic"
					}
				]
			},
			"footer": {
				"top": "Our bond advisors will respond within 48 hours to help you with this bond applicaiton, if you want to contact us feel free to call us on:",
				"bottom": "<a href='tel:0121231234' className='link semi-bold'>(012) 123 1234</a> or <a className='link semi-bold' href='mailto:hello@bondspark.com'>hello@bondspark.com</a>"
			}
		}

		return (

			<Step input={data}>
				<WizardBox data={data} />
			</Step>

		);

	}

}

export default BondAdvisor;
