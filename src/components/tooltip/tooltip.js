
import React, { Component } from 'react';

import profile from '../../assets/image/profile.jpeg';
import './tooltip.scss';

class Tooltip extends Component {

	state = {
		visible: true
	};

	constructor(props) {

		super(props);

		this.toggleTooltip = this.toggleTooltip.bind(this);

	}

	toggleTooltip() {

		this.setState({
			visible: false
		});

	}

	render() {

		return (

			<div className={
				this.state.visible ? 'tooltip-box fade-in' : 'tooltip-box tooltip-box--hidden'}>
				
				<div className="tooltip-box__inner">
						
					<img src={profile} 
						className="tooltip-box__image"
						alt="Profile"/>
					
					<div className="tooltip-box__text">

						TIP:

						<span className="tooltip-box__text--tip">
							You can leave at any time and we’ll send you an email to help you pick up where you left off.
						</span>

					</div>

					<div className="tooltip-box__text tooltip-box__text--footer"
						onClick={this.toggleTooltip}>

					 	<i className="material-icons tooltip-box__icon">
							thumb_up
						</i>

						GOT IT THANKS!

					</div>

				</div>

			</div>

		);

	}

}

export default Tooltip;
