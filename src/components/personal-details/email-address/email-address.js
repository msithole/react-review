
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	updateProgress,
	updateProgressLocation
} from '../../../redux/actions/app-actions';
import WizardInput from '../../wizard-input/wizard-input';
import Step from '../../step/step';

class EmailAddress extends Component {

	state = {};

	componentDidMount() {

		this.props.dispatch(updateProgress(3));
		this.props.dispatch(updateProgressLocation(this.props.location.pathname));

	}

	render() {

		const data = {
			"endpoint": "customer/apply/email",
			"animation": "/animation/animation1.json",
			"name": "email",
			"destination": "name-surname",
			"id": 1,
			"intro": "Now that we know you",
			"question": "What's your email address?",
			"type": "input",
			"button": {
				"type": "icon",
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "icon",
					"position": ""
				}
			},
			"actions": [
				{
					"placeholder": "Email Address",
					"id": "email",
					"type": "email",
					"validate": true,
					"validation": {
						"required": true,
						"pass": "That looks legit!",
						"fail": "Something doesn't look right"
					}
				}
			]
		};

		return (

			<Step input={data}>
				<WizardInput data={data} />
			</Step>


		);

	}

}


function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(EmailAddress);
