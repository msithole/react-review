
import React, { Component } from 'react';

import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class Residency extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/update",
			"animation": "/animation/animation1.json",
			"name": "permanentResident",
			"id": 9,
			"intro": "Are you a",
			"question": "permanent resident?",
			"type": "radio",
			"actions": [
				{
					"text": "Permanent",
					"id": "R1",
					"destination": "address",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Work Visa",
					"id": "R2",
					"destination": "address",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default Residency;
