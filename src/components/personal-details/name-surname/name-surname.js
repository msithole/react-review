
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {updateProgress} from '../../../redux/actions/app-actions';
import WizardInput from '../../wizard-input/wizard-input';
import Step from '../../step/step';

class NameSurname extends Component {

	state = {};

	componentDidMount() {

		this.props.dispatch(updateProgress(10));

	}

	render() {

		const data = {
			"endpoint": "customer/update",
			"animation": "/animation/animation2.json",
			"name": "nameSurname",
			"destination": "wedding-bells",
			"id": 2,
			"intro": "Welcome to Bondspark",
			"question": "What's your name?",
			"type": "input",
			"button": {
				"type": "icon",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "icon",
					"position": ""
				}
			},
			"actions": [
				{
					"placeholder": "First Name",
					"id": "name",
					"type": "text",
					"validation": {
						"required": false,
						"pass": "",
						"fail": ""
					}
				},
				{
					"placeholder": "Last Name",
					"type": "text",
					"id": "surname",
					"validation": {
						"required": false,
						"pass": "",
						"fail": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardInput data={data} />
			</Step>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(NameSurname);
