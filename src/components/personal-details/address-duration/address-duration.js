
import React, { Component } from 'react';
import WizardNumberPicker from '../../wizard-number-picker/wizard-number-picker';
import Step from '../../step/step';

class AddressDuration extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/address/update",
			"animation": "/animation/animation5.json",
			"name": "addressDuration",
			"id": 13,
			"destination": "/qualifications/highest-qualification",
			"intro": "How long",
			"question": "Have you lived here?",
			"type": "numberPicker",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"sets": [
				{
					"title": "Years",
					"length": 9
				},
				{
					"title": "Months",
					"length": 12
				}
			]
		};

		return (

			<Step input={data}>
				<WizardNumberPicker data={data} />
			</Step>

		);

	}

}

export default AddressDuration;
