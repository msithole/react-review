
import React, { Component } from 'react';

import WizardCountrySelector from '../../wizard-country-selector/wizard-country-selector';
import Step from '../../step/step';

class Nationality extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/update",
			"animation": "/animation/animation1.json",
			"name": "nationality",
			"id": 10,
			"intro": "Welcome to South Africa!",
			"question": "What is your Nationality?",
			"type": "countrySelector",
			"title": "Select Country:",
			"defaultValue": 249,
			"destination": "residency",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			}
		}

		return (

			<Step input={data}>
				<WizardCountrySelector data={data} />
			</Step>

		);

	}

}

export default Nationality;
