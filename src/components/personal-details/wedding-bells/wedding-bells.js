
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class WeddingBells extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/update",
			"animation": "/animation/animation3.json",
			"name": "maritalStatus",
			"id": 3,
			"intro": "Wedding Bells",
			"question": "Are you married?",
			"type": "radio",
			"actions": [
				{
					"text": "Nope",
					"id": "wb1",
					"destination": "multiple-applicants",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Yes, in community of property",
					"id": "wb2",
					"destination": "besides-spouse",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Yes, <strong>with<strong> Accrual",
					"id": "wb3",
					"destination":"besides-spouse",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Yes, <strong>without</strong> Accrual",
					"id": "wb4",
					"destination": "multiple-applicants",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default WeddingBells;
