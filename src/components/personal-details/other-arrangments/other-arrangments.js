
import React, { Component } from 'react';

import WizardTextArea from '../../wizard-textarea/wizard-textarea';
import Step from '../../step/step';

class OtherArrangements extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/address/update",
			"animation": "/animation/animation5.json",
			"name": "description",
			"id": 14,
			"destination": "address-duration",
			"intro": "OK Something else,",
			"question": "Describe the situation",
			"type": "textarea",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"textarea": {
				"title": "Type a description:",
				"placeholder": "Write something...",
				"validation": {
					"required": true,
					"pass": "That looks legit!",
					"fail": "Aren't you forgetting something - this field is required"
				}
			}
		};

		return (

			<Step input={data}>
				<WizardTextArea data={data} />
			</Step>

		);

	}

}

export default OtherArrangements;
