
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class IdPassport extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/address/update",
			"animation": "/animation/animation5.json",
			"name": "residenceStatus",
			"id": 12,
			"intro": "What is your current",
			"question": "residence situation?",
			"type": "radio",
			"actions": [
				{
					"text": "Tenant",
					"id": "LA1",
					"destination": "address-duration",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Owner",
					"id": "LA2",
					"destination": "address-duration",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Living with Parents",
					"id": "LA3",
					"destination": "address-duration",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Other...",
					"id": "LA4",
					"destination": "other-arrangments",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		};

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default IdPassport;
