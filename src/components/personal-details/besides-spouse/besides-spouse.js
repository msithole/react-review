
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class BesidesSpouse extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "application/update",
			"animation": "/animation/animation4.json",
			"name": "anyoneElseApplying",
			"id": 5,
			"intro": "Besides your spouse,",
			"question": "Is anyone else applying?",
			"type": "radio",
			"actions": [
				{
					"text": "<strong>No</strong>, just my spouse and I",
					"id": "BS1",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Yes, there is <strong>someone else<strong>",
					"id": "BS2",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default BesidesSpouse;
