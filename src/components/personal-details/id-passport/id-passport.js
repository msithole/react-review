
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
	updateProgress,
	updateProgressLocation
} from '../../../redux/actions/app-actions';
import WizardInput from '../../wizard-input/wizard-input';
import Step from '../../step/step';

class IdPassport extends Component {

	componentDidMount() {

		this.props.dispatch(updateProgress(1));
		this.props.dispatch(updateProgressLocation(this.props.location.pathname));

	}

	render() {

		const data = {
			"animation": "/animation/animation1.json",
			"name": "idNumber",
			"destination": "email-address",
			"id": 1,
			"intro": "Let's start with an",
			"question": "ID or Passport Number.",
			"type": "input",
			"button": {
				"type": "icon",
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "icon",
					"position": ""
				}
			},
			"actions": [
				{
					"placeholder": "Enter ID Number",
					"id": "idNumber",
					"type": "text",
					"validate": true,
					"validation": {
						"required": true,
						"pass": "That looks legit!",
						"fail": "Something doesn't look right"
					}
				}
			]
		};

		return (

			<Step input={data}>
				<WizardInput data={data} />
			</Step>


		);

	}

}


function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(IdPassport);
