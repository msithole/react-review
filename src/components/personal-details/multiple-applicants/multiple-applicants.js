
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class MultipleApplicants extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "application/update",
			"animation": "/animation/animation4.json",
			"name": "anyOtherApplicants",
			"id": 4,
			"intro": "OK Great!",
			"question": "Are there any other applicants?",
			"type": "radio",
			"actions": [
				{
					"text": "No, <strong>just me</strong>",
					"id": "MA1",
					"destination": "citizenship",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Yes, there is <strong>someone else<strong>",
					"id": "MA2",
					"destination": "more-applicants",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default MultipleApplicants;
