
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class Citizenship extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/update",
			"animation": "/animation/animation1.json",
			"name": "saCitizen",
			"id": 6,
			"intro": "We're based in South Africa.",
			"question": "Are you a South African citizen?",
			"type": "radio",
			"actions": [
				{
					"text": "<strong>Yes</strong> I am :)",
					"id": "C1",
					"destination": "residency",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "<strong>No</strong>, I'm a foreign citizen",
					"id": "C2",
					"destination": "nationality",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default Citizenship;
