
import React, { Component } from 'react';
import WizardTextArea from '../../wizard-textarea/wizard-textarea';
import Step from '../../step/step';

class Address extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/address/update",
			"animation": "/animation/animation5.json",
			"name": "fullAddress",
			"id": 11,
			"destination": "living-arrangements",
			"intro": "Home sweet home",
			"question": "Where do you live?",
			"type": "textarea",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"textarea": {
				"title": "Enter your address:",
				"placeholder": "e.g. 12a Marshall Eagle, Amberfield Heights, Rooihuiskraal, 1020",
				"validation": {
					"required": true,
					"pass": "That looks legit!",
					"fail": "Aren't you forgetting something - this field is required"
				}
			}
		}

		return (

			<Step input={data}>
				<WizardTextArea data={data} />
			</Step>

		);

	}

}

export default Address;
