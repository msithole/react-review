
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class MoreApplicants extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "application/update",
			"animation": "/animation/animation4.json",
			"name": "howManyApplicants",
			"id": 7,
			"intro": "So you're applying with others",
			"question": "How many other applicants?",
			"type": "radio",
			"actions": [
				{
					"text": "Just <strong>1 other</strong> applicant",
					"id": "OA1",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "<strong>2<strong>, or more",
					"id": "OA2",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default MoreApplicants;
