
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {updateProgress} from '../../../redux/actions/app-actions';
import WizardInputTextarea from '../../wizard-input-textarea/wizard-input-textarea';
import Step from '../../step/step';

class PreviousCompany extends Component {

	componentDidMount() {

		this.props.dispatch(updateProgress(10));

	}

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "company",
			"destination": "previous-company-duration",
			"id": 23,
			"intro": `For ${this.props.app.form.previousEmployer},`,
			"question": "we'll need more details",
			"type": "input-textarea",
			"button": {
				"type": "icon",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "icon",
					"position": ""
				}
			},
			"input": {
				"placeholder": "e.g 012 231 123",
				"label": "Employer Number:",
				"id": "previousEmployerContact",
				"name": "previousEmployerContact",
				"type": "text",
				"validation": {
					"required": true,
					"pass": "That looks legit!",
					"fail": "Something doesn't look right"
				}
			},
			"textarea": {
				"name": "previousEmployerAddress",
				"id": "previousEmployerAddress",
				"label": "Business Address:",
				"placeholder": "e.g. 284 Oak Ave, Ferndale",
				"col": "20",
				"row": "50",
				"validation": {
					"required": true,
					"pass": "That looks legit!",
					"fail": "Aren't you forgetting something - this field is required"
				}
			}

		}

		return (

			<Step input={data}>
				<WizardInputTextarea data={data} />
			</Step>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(PreviousCompany);
