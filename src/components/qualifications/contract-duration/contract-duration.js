
import React, { Component } from 'react';
import WizardNumberPicker from '../../wizard-number-picker/wizard-number-picker';
import Step from '../../step/step';

class ContractDuration extends Component {

	state = {
		company: 'COMPANY'
	};

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "contractDuration",
			"id": 33,
			"destination": "company-duration",
			"intro": "How long is",
			"question": `your contract at ${this.state.company}?`,
			"type": "numberPicker",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"sets": [
				{
					"title": "Years",
					"length": 9
				},
				{
					"title": "Months",
					"length": 12
				}
			]
		};

		return (

			<Step input={data}>
				<WizardNumberPicker data={data} />
			</Step>

		);

	}

}

export default ContractDuration;
