
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class PreviousEmployer extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "previousEmployed",
			"id": 25,
			"intro": "We need to know a little more",
			"question": "about your previous employer",
			"type": "radio",
			"actions": [
				{
					"text": "Okay, sure!",
					"id": "pe1",
					"destination": "previous-employer-details",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "I don't have a previous employer",
					"id": "pe2",
					"destination": "/",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default PreviousEmployer;
