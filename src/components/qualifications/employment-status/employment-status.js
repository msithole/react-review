
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class EmploymentStatus extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "employmentStatus",
			"id": 21,
			"intro": "What is your",
			"question": "employment status?",
			"type": "radio",
			"progress": 9,
			"actions": [
				{
					"text": "Full Time Employee",
					"id": "es1",
					"destination": "employer",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Part Time Employee",
					"id": "es2",
					"destination": "employer",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Self Employed",
					"id": "es3",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Unemployed",
					"id": "es4",
					"destination": "/bond-advisor",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default EmploymentStatus;
