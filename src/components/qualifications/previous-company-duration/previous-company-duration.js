
import React, { Component } from 'react';
import { connect } from 'react-redux';

import WizardNumberPicker from '../../wizard-number-picker/wizard-number-picker';
import Step from '../../step/step';

class PreviousCompanyDuration extends Component {

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "previousDuration",
			"id": 38,
			"destination": "/",
			"intro": "How long",
			"question": `did you work at ${this.props.app.form.previousEmployer}?`,
			"type": "numberPicker",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"sets": [
				{
					"title": "Years",
					"length": 9
				},
				{
					"title": "Months",
					"length": 12
				}
			]
		};

		return (

			<Step input={data}>
				<WizardNumberPicker data={data} />
			</Step>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(PreviousCompanyDuration);
