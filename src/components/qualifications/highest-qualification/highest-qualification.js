
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class HighestQualification extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/education",
			"animation": "/animation/animation7.json",
			"name": "qualificationLevel",
			"id": 11,
			"intro": "Academically what is your",
			"question": "highest qualification?",
			"type": "radio",
			"progress": 20,
			"actions": [
				{
					"text": "Pre-matric",
					"id": "hq1",
					"destination": "pre-matric",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Matric GR.12",
					"id": "hq2",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Diploma",
					"id": "hq3",
					"destination": "diploma-duration",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Degree",
					"id": "hq4",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Honours",
					"id": "hq5",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Doctrate",
					"id": "hq6",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "Other",
					"id": "hq7",
					"destination": "describe-qualification",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default HighestQualification;
