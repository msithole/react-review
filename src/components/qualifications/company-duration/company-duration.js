
import React, { Component } from 'react';
import { connect } from 'react-redux';

import WizardNumberPicker from '../../wizard-number-picker/wizard-number-picker';
import Step from '../../step/step';

class CompanyDuration extends Component {

	state = {};

	constructor (props){

		super(props);

		this.decidedOnDestination = this.decidedOnDestination.bind(this);

	}

	decidedOnDestination(duration){

		if(this.props.form.employmentStatus === 'es2') {

			return '/';

		} else {

			return duration.Years > 2 ? '/' : null

		}

	}

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "currentDuration",
			"id": 13,
			"destination": "previous-employer",
			"intro": "How long",
			"question": "Have you worked here?",
			"type": "numberPicker",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"sets": [
				{
					"title": "Years",
					"length": 9
				},
				{
					"title": "Months",
					"length": 12
				}
			]
		};

		return (

			<Step input={data}>
				<WizardNumberPicker
					data={data}
					decidedOnDestination={this.decidedOnDestination}>
				</WizardNumberPicker>
			</Step>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default connect(mapStateToProps)(CompanyDuration);
