
import React, { Component } from 'react';

import WizardTextArea from '../../wizard-textarea/wizard-textarea';
import Step from '../../step/step';

class describeQualification extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/education",
			"animation": "/animation/animation7.json",
			"name": "qualificationDescription",
			"id": 24,
			"destination": "employment-status",
			"intro": "OK Something else,",
			"question": "Describe the qualification",
			"type": "textarea",
			"button": {
				"type": "basic",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "basic",
					"position": "right"
				}
			},
			"textarea": {
				"title": "Type a description:",
				"id": "qualificationDescription",
				"placeholder": "Write something...",
				"validation": {
					"required": true,
					"pass": "That looks legit!",
					"fail": "Aren't you forgetting something - this field is required"
				}
			}
		};

		return (

			<Step input={data}>
				<WizardTextArea data={data} />
			</Step>

		);

	}

}

export default describeQualification;
