
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {updateProgress} from '../../../redux/actions/app-actions';
import WizardInput from '../../wizard-input/wizard-input';
import Step from '../../step/step';

class PreviousEmployerDetails extends Component {

	state = {};

	componentDidMount() {

		this.props.dispatch(updateProgress(10));

	}

	render() {

		const data = {
			"endpoint": "customer/employment",
			"animation": "/animation/animation8.json",
			"name": "employer",
			"destination": "previous-company",
			"id": 22,
			"intro": "Who was your",
			"question": "previous employer?",
			"type": "input",
			"button": {
				"type": "icon",
				"trigger": true,
				"text": "Next",
				"icon": {
					"name": "arrow_forward",
					"type": "icon",
					"position": ""
				}
			},
			"actions": [
				{
					"placeholder": "e.g Leadhome",
					"label": "Employer name:",
					"id": "previousEmployer",
					"name": "previousEmployer",
					"type": "text",
					"validation": {
						"required": true,
						"pass": "",
						"fail": ""
					}
				},
				{
					"placeholder": "e.g Local Property Expert",
					"label": "Your position:",
					"type": "text",
					"id": "previousPosition",
					"name": "previousPosition",
					"validation": {
						"required": true,
						"pass": "",
						"fail": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardInput data={data} />
			</Step>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app
	}

}

export default connect(mapStateToProps)(PreviousEmployerDetails);
