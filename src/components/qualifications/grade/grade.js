
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class Grade extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/education",
			"animation": "/animation/animation7.json",
			"name": "studyLevel",
			"id": 20,
			"intro": "So you finished before matric",
			"question": "in which grade?",
			"type": "radio",
			"actions": [
				{
					"text": "<strong>Before</strong> Gr.10",
					"id": "g1",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "<strong>After</strong> Gr.10",
					"id": "g2",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default Grade;
