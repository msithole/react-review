
import React, { Component } from 'react';
import WizardRadio from '../../wizard-radio/wizard-radio';
import Step from '../../step/step';

class DiplomaDuration extends Component {

	state = {};

	render() {

		const data = {
			"endpoint": "customer/education",
			"animation": "/animation/animation7.json",
			"name": "studyLevel",
			"id": 20,
			"intro": "For the diploma,",
			"question": "how many years did you study?",
			"type": "radio",
			"actions": [
				{
					"text": "<strong>1</strong> Year",
					"id": "dd1",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "<strong>2</strong> Years",
					"id": "dd2",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				},
				{
					"text": "<strong>3</strong> Years",
					"id": "dd3",
					"destination": "employment-status",
					"type": "border",
					"icon": {
						"name": "",
						"type": "",
						"position": ""
					}
				}
			]
		}

		return (

			<Step input={data}>
				<WizardRadio data={data} />
			</Step>

		);

	}

}

export default DiplomaDuration;
