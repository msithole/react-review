
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {postData} from '../../redux/actions/app-actions';
import Button from '../button/button';
import Textarea from '../textarea/textarea';
import './wizard-textarea.scss';

class WizardTextarea extends Component {

	state = {
		valid: 'fail',
		form: {},
		isStepValid: false,
		showValidation: false
	}

	constructor (props){

		super(props);

		this.onBlur = this.onBlur.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onClick = this.onClick.bind(this);

	}

	componentDidMount() {

		this.setState({
			form: this.props.form
		});

		if(this.props.form.hasOwnProperty(this.props.data.name)) {
			this.validateStep(this.props.form)
		}

	}

	onChange(e) {

		this.setState({
			form: {
				[this.props.data.name]: e.target.value
			}
		});
		
	}

	onBlur(e) {

		if(e.target.value.length > 0) {

			this.setState({
				valid: 'pass',
				showValidation: true,
				isStepValid: true
			});

		} else {

			this.setState({
				valid: 'fail',
				isStepValid: false,
				showValidation: true,
			});

		}

	}

	onClick() {

		this.setState({
			showValidation: true
		});

		if(this.state.isStepValid) {

			this.props.dispatch(postData(this.state.form, this.props.data.endpoint || ''));
			this.props.history.push(`${this.props.data.destination}`);

		}

	}

	validateStep(form) {

		if(form.hasOwnProperty(this.props.data.name)){

			this.setState({
				valid: 'pass',
				isStepValid: true,
				showValidation: true
			});

		}

	}

	render() {

		return (

			<>

				<div className="form__item">

					<p className="form__label">
						{this.props.data.textarea.title}
					</p>

					<Textarea
						value={this.state.form[this.props.data.name]}
						name={this.props.data.name}
						placeholder={this.props.data.textarea.placeholder}
						onChange={this.onChange}
						onBlur={this.onBlur}>
					</Textarea>

				</div>

				<div className="personal-details__item d-flex justify-content-end"
					onClick={this.onClick}>

					<Button {...this.props.data.button}
						disabled={!this.state.isStepValid}/>

				</div>

			</>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(WizardTextarea));
