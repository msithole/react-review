
import React,{ Component } from 'react';
import { connect } from 'react-redux'
import Swiper from 'react-id-swiper';
import 'react-id-swiper/src/styles/scss/swiper.scss';

import './number-picker.scss';

class NumberPicker extends Component {

	state = {
		render: true
	}

	constructor(props) {

		super(props);
		this.swiper = null;

	}

	onSliderInit () {

		setTimeout(() => {

			if(this.props.selected) {

				if(this.swiper) {
					this.swiper.slideTo(this.props.selected[this.props.name], 3000)
				}

			}
			
		}, 0);

	}

	onClick(e) {

		console.log(this.swiper.clickedIndex)

	}

	render() {

		const params = {
			containerClass: 'number-slider__carousel',
			slideClass: 'number-slider__item',
			slideActiveClass: 'number-slider__item--active',
			slidePrevClass: 'number-slider__item--prev',
			slideNextClass: 'number-slider__item--next',
			slideToClickedSlide: true,
			preventClicks: false,
			preventClicksPropagation: false,
			centeredSlides: true,
			slidesPerView: 10,
			spaceBetween: 25,
			on: {
				slideChange: () => {
					this.swiper.name = this.props.name;
					this.props.onChange(this.swiper);
				},
				click: () => {
					this.onClick()
				},
				init: () => {
					this.onSliderInit()
				}
			}
		}

		return (

			this.state.render ?
			
			<Swiper shouldSwiperUpdate
				{...params}
				ref={(el) => {

					if (el) {
						this.swiper = el.swiper
					}

				}}>

				{this.props.items}

			</Swiper> : 

			null

		);

	}

}

function mapStateToProps(state) {

    return {
        forms: state.forms,
	}

}

export default connect(mapStateToProps)(NumberPicker)
