
import React, { Component } from 'react';
import { connect } from 'react-redux';

import './progress-bar.scss';

class ProgressBar extends Component {

	render() {

		const stateProgress = parseInt(this.props.app.progress);
		const progressArray = [...Array(stateProgress).keys()];

		const progressItems = progressArray.map((progress) =>

			<div key={progress}
				className={progress + 1 !== progressArray.length ? 'progressbar__block' : 'progressbar__block progressbar__block--active'}>
			</div>

		);
	
		return (

			<div className="progressbar">
				{progressItems}
			</div>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default connect(mapStateToProps)(ProgressBar)
