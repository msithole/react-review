
import React, { Component } from 'react';

import './textarea.scss';

class Textarea extends Component {

	render() {

		return (

			<>
			
				{this.props.label && 

					<label className="form__input-label">
						{this.props.label}
					</label>

				}
				<textarea
					className="form__textbox"
					id={this.props.id}
					name={this.props.name}
					col={this.props.col}
					row={this.props.row}
					value={this.props.value || ''}
					placeholder={this.props.placeholder}
					onBlur={this.props.onBlur}
					onChange={this.props.onChange}>
				</textarea>

			</>

		);

	}

}

export default Textarea;
