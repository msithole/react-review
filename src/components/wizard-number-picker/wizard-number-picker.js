
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {postData} from '../../redux/actions/app-actions';
import Button from '../button/button';
import NumberPicker from '../number-picker/number-picker';
import './wizard-number-picker.scss';

class WizardNumberPicker extends Component {

	state = {
		form: {},
		showSlider: false,
		isStepValid: false
	}

	constructor (props){

		super(props);

		this.onChange = this.onChange.bind(this);
		this.onClick = this.onClick.bind(this);

	}

	componentDidMount() {

		this.setState({
			form: this.props.form,
			showSlider: true
		});

	}

	onChange(ev) {

		this.setState({
			isStepValid: true,
			form: {
				[this.props.data.name]: {
					...this.state.form[this.props.data.name],
					[ev.name]: ev.activeIndex
				}
			}
		});
		
	}

	onClick() {

		if(this.state.isStepValid) {

			this.props.dispatch(postData(this.state.form, this.props.data.endpoint || ''));

			this.props.decidedOnDestination ? 
			this.props.history.push( this.props.decidedOnDestination(this.state.form.currentDuration) || this.props.data.destination) : 
			this.props.history.push(`${this.props.data.destination}`);

		}

	}
 
	render() {

		const numberPickerItem = this.props.data.sets.map((item, index) => {
		
			const SliderItems = [...Array(item.length + 1).keys()].map((number, index) => 
	
				<div key={index}>
					{number}
				</div>

			);

			return <div className="number-slider"
				key={index}>

				<div className="number-slider__marker"></div>

				{this.state.showSlider && 
					<NumberPicker
						selected={this.state.form ? this.state.form[this.props.data.name] : null}
						name={item.title}
						onChange={this.onChange}
						items={SliderItems}>
					</NumberPicker>
				}

				<label>
					{item.title}
				</label>

			</div>

		});

		return (

			<>

				{numberPickerItem}

				<div className="personal-details__item d-flex justify-content-end"
					onClick={this.onClick}>

					<Button {...this.props.data.button}
						disabled={!this.state.isStepValid}/>

				</div>

			</>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(WizardNumberPicker));
