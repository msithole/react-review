
import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { continueApplication } from '../../redux/actions/app-actions';
import logo from '../../assets/image/logo.svg';
import background from '../../assets/image/error-500-background.svg';

class ContinueApplication extends Component {

	componentDidMount() {
 
		this.props.dispatch(continueApplication(this.props.location.search.substring(10)));

	}

	render() {

		return (

			<div className="error page">

				<div className="error__content d-flex flex-column justify-content-around">

					<div>
	
						<img src={background} 
							className="error__background"
							alt="Bondspark"/>

						<h2 className="error__title">

							Whooaaah! We busy running through all the files

							<div className="semi-bold">
								Please wait while we find yours...
							</div>

						</h2>

						<label>Continue Applicaiton</label>

					</div>

				</div>

				<footer className="error__footer d-flex flex-column align-items-center">

					<img src={logo} 
						className="error__logo"
						alt="Bondspark"/>

				</footer>

			</div>

		);

	}

}

function mapStateToProps(state) {

    return {
		app: state.app,
		form: state.app.form
	}

}

export default withRouter(connect(mapStateToProps)(ContinueApplication));
