
import React, { Component } from 'react';
import Parser from 'html-react-parser';
import { connect } from 'react-redux';

import './button.scss';

import logo from '../../assets/image/logo.svg';

class Button extends Component {

	componentDidMount() {}

	render() {

		return (

			<>
				
				<div className={`
						button
						${this.props ? 'button--' + this.props.type : ''}
						${this.props.app.isPosting ? 'button--' + this.props.type + '-loading' : ''} 
						${this.props.icon ? 'button--' + this.props.icon.position : ''}
						${this.props && this.props.selectedOption ===  this.props.id ? 'button--border-active' : ''}
						${this.props.disabled ? 'button--deactive' : ''}
					`}
					onClick={this.props.onClick}>

					<span className={`${this.props ? 'button--desktop-' + this.props.type : ''}`}>
						{Parser(this.props ? this.props.text : '')}
					</span>
					
					{this.props.app.isPosting ?

						null

					: 
					
					<i className={`material-icons button__icon button__icon--${this.props.icon ? this.props.icon.type : ''}`}>
						{this.props.icon ? this.props.icon.name : ''}
					</i>}
					
				</div>

			</>

		);

	}

}

function mapStateToProps(state) {

    return {
        app: state.app,
	}

}

export default connect(mapStateToProps)(Button);
