
import 'react-app-polyfill/ie9';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import store, { history } from './redux/create-store';
import * as serviceWorker from './serviceWorker';
import App from './app';

ReactDOM.render(

	<Provider store={store}>

		<ConnectedRouter history={history}>

			<App />

		</ConnectedRouter>

	</Provider>,

	document.getElementById('bondspark')

);

serviceWorker.register();
